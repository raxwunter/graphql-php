<?php
namespace BlogTest;

header('Access-Control-Allow-Credentials: false', true);
header('Access-Control-Allow-Origin: *');
if ($_SERVER['REQUEST_METHOD'] == "OPTIONS") {
    return;
}

use Examples\Blog\Schema\BlogSchema;
use Youshido\GraphQL\Execution\Processor;
use Youshido\GraphQL\Schema\Schema;


if (preg_match('/\.css$/', $_SERVER["REQUEST_URI"])) {
    header('Content-Type: text/css');
    echo file_get_contents('./GraphiQL'.$_SERVER["REQUEST_URI"]);
    return;
}

if (preg_match('/\/graphql$/', $_SERVER["REQUEST_URI"]) && $_SERVER["REQUEST_METHOD"] === 'GET') {
    //print_r($_SERVER);
    echo file_get_contents('./GraphiQL/index.html');
    return;
}

require_once __DIR__ . '/schema-bootstrap.php';
/** @var Schema $schema */
$schema = new BlogSchema();


if ($_SERVER["REQUEST_METHOD"] === 'POST') {
    $rawBody     = file_get_contents('php://input');
    //echo $rawBody;
    $requestData = json_decode($rawBody ?: '', true);
} else {
    $requestData = $_POST;
}

$payload   = isset($requestData['query']) ? $requestData['query'] : null;
$variables = isset($requestData['variables']) ? $requestData['variables'] : null;

$processor = new Processor($schema);

$response = $processor->processPayload($payload, $variables)->getResponseData();

header('Content-Type: application/json');
echo json_encode($response);
